﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/physicalExamination")]
    public class PhysicalExaminationController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IPhysicalExaminationService physicalExaminationService;

        public PhysicalExaminationController(IUserProviderService userProviderService,
            IUserService userService,
            IPhysicalExaminationService physicalExaminationService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.physicalExaminationService = physicalExaminationService;
        }

        [Authorize]
        [HttpPost("doctor")]
        public IActionResult AddPhysicalExamination([FromBody] PhysicalExaminationDTO newPhysicalExaminationDTO)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            {
                physicalExaminationService.AddPhysicalExamination(newPhysicalExaminationDTO);
                return Ok("Dodano badanie pomyślnie");
            }
            else
            {
                return BadRequest("Brak uprawnien lekarza");
            }
        }

        [Authorize]
        [HttpGet("doctor/patient/{patientId}")]
        public IActionResult GetPhysicalExamination(int patientId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            { 
                PhysicalExaminationListDTO physicalExaminationListDTO = physicalExaminationService.GetPhysicalExaminationsByPatientId(patientId);
                return Ok(physicalExaminationListDTO);
            }
            else
            {
                return BadRequest("Brak uprawnien lekarza");
            }
        }
    }
}


