﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/laboratoryTest")]
    public class LaboratoryTestController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly ILaboratoryTestService laboratoryTestService;

        public LaboratoryTestController(IUserProviderService userProviderService,
            IUserService userService,
            ILaboratoryTestService laboratoryTestService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.laboratoryTestService = laboratoryTestService;
        }

        [Authorize]
        [HttpGet("doctor/patient/{patientId}")]
        public IActionResult GetLaboratoryTestByPatientId(int patientId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            {
                LaboratoryTestListDTO laboratoryTestListDTO = laboratoryTestService.GetLaboratoryTestByPatientId(patientId);
                return Ok(laboratoryTestListDTO);
            }
            else
            {
                return BadRequest("Brak uprawnien doktora");
            }
        }

        [Authorize]
        [HttpPatch("laboratoryAssistant/make/{laboratoryTestId}")]
        public IActionResult MakeLaboratoryTest(int laboratoryTestId, [FromBody] MakeAcceptCancelLaboratoryTestDTO makelaboratoryTestDTO)
        {

            int userId = userProviderService.GetUserId();
            if (userService.IsLaboratoryAssistant(userId))
            {
                int laboratoryAssistantId = userService.GetLaboratoryAssistantIdByUserId(userId);
                laboratoryTestService.MakeCancelLaboratoryTest(laboratoryTestId, makelaboratoryTestDTO, laboratoryAssistantId, "WYK");
                return Ok("Pomyslnie przeprowadzono badanie");
            }
            else
            {
                return BadRequest("Brak uprawnien asystenta laboratorium");
            }
        }

        [Authorize]
        [HttpPatch("laboratoryAssistant/cancel/{laboratoryTestId}")]
        public IActionResult CancelLaboratoryTestAsLaboratoryAssistant(int laboratoryTestId, [FromBody] MakeAcceptCancelLaboratoryTestDTO makelaboratoryTestDTO)
        {

            int userId = userProviderService.GetUserId();
            if (userService.IsLaboratoryAssistant(userId))
            {
                int laboratoryAssistantId = userService.GetLaboratoryAssistantIdByUserId(userId);
                laboratoryTestService.MakeCancelLaboratoryTest(laboratoryTestId, makelaboratoryTestDTO, laboratoryAssistantId, "ANULA");
                return Ok("Pomyslnie anulowano badanie");
            }
            else
            {
                return BadRequest("Brak uprawnien asystenta laboratorium");
            }
        }

        [Authorize]
        [HttpPatch("laboratorySupervisor/accept/{laboratoryTestId}")]
        public IActionResult AcceptLaboratoryTest(int laboratoryTestId, [FromBody] MakeAcceptCancelLaboratoryTestDTO acceptLaboratoryTestDTO)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsLaboratorySupervisor(userId))
            {
                int laboratorySupervisorId = userService.GetLaboratorySupervisorIdByUserId(userId);
                laboratoryTestService.AcceptCancelLaboratoryTest(laboratoryTestId, acceptLaboratoryTestDTO, laboratorySupervisorId, "ZATW");
                return Ok("Pomyslnie zatwierdzono badanie");
            }
            else
            {
                return BadRequest("Brak uprawnien kierownika laboratorium");
            }
        }

        [Authorize]
        [HttpPatch("laboratorySupervisor/cancel/{laboratoryTestId}")]
        public IActionResult CancelLaboratoryTestAsLaboratorySupervisor(int laboratoryTestId, [FromBody] MakeAcceptCancelLaboratoryTestDTO acceptLaboratoryTestDTO)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsLaboratorySupervisor(userId))
            {
                int laboratorySupervisorId = userService.GetLaboratorySupervisorIdByUserId(userId);
                laboratoryTestService.AcceptCancelLaboratoryTest(laboratoryTestId, acceptLaboratoryTestDTO, laboratorySupervisorId, "ANULK");
                    return Ok("Pomyslnie anulowano badanie");
            }
            else
            {
                return BadRequest("Brak uprawnien kierownika laboratorium");
            }
        }

        [Authorize]
        [HttpGet("laboratoryAssistant/testId/{laboratoryTestId}")]
        public IActionResult GetLaboratoryTestById(int laboratoryTestId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsLaboratoryAssistant(userId))
            {
                LaboratoryTestDTO laboratoryTestDTO = laboratoryTestService.GetLaboratoryTestById(laboratoryTestId);
                return Ok(laboratoryTestDTO);
            }
            else
            {
                return BadRequest("Brak uprawnien asystenta laboratorium");
            }
        }

        [Authorize]
        [HttpGet("")]
        public IActionResult GetLaboratoryTests([FromQuery] string status)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsLaboratoryAssistant(userId) || userService.IsLaboratorySupervisor(userId))
            {
                LaboratoryTestListDTO laboratoryTestListDTO;
                if (string.IsNullOrEmpty(status))
                {
                    laboratoryTestListDTO = laboratoryTestService.GetLaboratoryTests();
                    return Ok(laboratoryTestListDTO);
                }
                else
                {
                    laboratoryTestListDTO = laboratoryTestService.GetLaboratoryTestsByStatus(status);
                    return Ok(laboratoryTestListDTO);
                }
            }
            else
            {
                return BadRequest("Brak uprawnien do podgladu testow");
            }
        }

        [Authorize]
        [HttpPost("doctor")]
        public IActionResult AddLaboratoryTest([FromBody] LaboratoryTestDTO newLaboratoryTestDTO)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            {
                laboratoryTestService.AddLaboratoryTest(newLaboratoryTestDTO);
                return Ok("Dodano badanie pomyślnie");
            }
            else
            {
                return BadRequest("Brak uprawnien lekarza");
            }
        }
    }
}
