﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/doctor")]
    public class DoctorController : ControllerBase
    {
        private readonly IDoctorService doctorService;
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;

        public DoctorController(
            IDoctorService doctorService,
            IUserProviderService userProviderService,
            IUserService userService)
        {
            this.doctorService = doctorService;
            this.userProviderService = userProviderService;
            this.userService = userService;
        }

        [Authorize]
        [HttpGet("registrant")]
        public IActionResult GetDoctors()
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsRegistrant(userId))
            {
                DoctorListDTO doctorListDTO = doctorService.GetDoctors();
                return Ok(doctorListDTO);
            }
            else
            {
                return BadRequest("Brak uprawnien rejestratora");
            }
        }
    }
}
