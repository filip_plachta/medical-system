﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/laboratoryTestStatus")]
    public class LaboratoryTestStatusController : ControllerBase
    {
        private readonly ILaboratoryTestStatusService laboratoryTestStatusService;

        public LaboratoryTestStatusController(
            ILaboratoryTestStatusService laboratoryTestStatusService)
        {
            this.laboratoryTestStatusService = laboratoryTestStatusService;
        }

        [Authorize]
        [HttpGet("")]
        public IActionResult GetAppointmentStatuses()
        {
            LaboratoryTestStatusListDTO laboratoryTestStatusListDTO = laboratoryTestStatusService.GetLaboratoryTestStatuses();
            return Ok(laboratoryTestStatusListDTO);
        }
    }
}
