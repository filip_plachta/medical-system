﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/userRole")]
    public class UserRoleController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IUserRoleService userRoleService;

        public UserRoleController(IUserProviderService userProviderService,
            IUserService userService,
            IUserRoleService userRoleService)
        {
            this.userProviderService = userProviderService;
            this.userService = userService;
            this.userRoleService = userRoleService;
        }

        [Authorize]
        [HttpGet("admin")]
        public IActionResult GetUserRoles()
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsAdmin(userId))
            {
                RoleListDTO roleListDTO = userRoleService.GetRoles();
                return Ok(roleListDTO);
            }
            else
            {
                return BadRequest("Brak uprawnien administratora");
            }
        }
    }
}
