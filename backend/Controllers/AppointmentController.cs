﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.ServicesCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace MedicalSystemBackend.Controllers
{
    [ApiController]
    [Route("api/appointment")]
    public class AppointmentController : ControllerBase
    {
        private readonly IUserProviderService userProviderService;
        private readonly IUserService userService;
        private readonly IAppointmentService appointmentService;
        private readonly IAppointmentStatusService appointmentStatusService;

        public AppointmentController(IUserProviderService userProviderService,
            IUserService userService,
            IAppointmentService appointmentService,
            IAppointmentStatusService appointmentStatusService)
        {
            this.userService = userService;
            this.userProviderService = userProviderService;
            this.appointmentService = appointmentService;
            this.appointmentStatusService = appointmentStatusService;
        }

        [Authorize]
        [HttpGet("doctor/patient/{patientId}")]
        public IActionResult GetAppointmentsByPatientId(int patientId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            {
                AppointmentListDTO appointments = appointmentService.GetAppointmentsByPatientId(patientId);
                return Ok(appointments);
            }
            else
            {
                return BadRequest("Brak uprawnien doktora");
            }
        }

        [Authorize]
        [HttpGet("registrant")]
        public IActionResult GetAppointments([FromQuery] int day, [FromQuery] int month, [FromQuery] int year, [FromQuery] int doctorId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsRegistrant(userId))
            {
                if (day == 0 || month == 0 || year == 0)
                {
                    return BadRequest("day, month i year nie moga byc rowne 0");
                }
                DateTime date = new DateTime(year, month, day);
                if (doctorId == 0)
                {
                    AppointmentListDTO appointmentListDTO = appointmentService.GetAppointmentsByDate(date);
                    return Ok(appointmentListDTO);
                }
                else
                {
                    AppointmentListDTO appointmentListDTO = appointmentService.GetDoctorsAppointmentsByDate(date, doctorId);
                    return Ok(appointmentListDTO);
                }
            }
            else
            {
                return BadRequest("Brak uprawnien rejestratora");
            }
        }

        [Authorize]
        [HttpGet("doctor")]
        public IActionResult GetDoctorsAppointments([FromQuery] int day, [FromQuery] int month, [FromQuery] int year, [FromQuery] string statusCode)
        {
            int userId = userProviderService.GetUserId();
            try
            {
                int doctorId = userService.GetDoctorIdByUserId(userId);

                if (day == 0 || month == 0 || year == 0)
                {
                    return BadRequest("Day, month i year nie moga byc rowne 0");
                }
                DateTime date = new DateTime(year, month, day);
                AppointmentListDTO appointmentListDTO;
                if (String.IsNullOrEmpty(statusCode))
                {
                    appointmentListDTO = appointmentService.GetDoctorsAppointmentsByDate(date, doctorId);
                }
                else
                {
                    appointmentListDTO = appointmentService.GetDoctorsAppointmentsByDateAndStatusCode(date, doctorId, statusCode);
                }
                return Ok(appointmentListDTO);
            }
            catch (MedicalSystemServerException exeption)
            {
                return BadRequest(exeption.Message);
            }
        }

        [Authorize]
        [HttpPost("registrant")]
        public IActionResult AddAppointment([FromBody] AppointmentDTO newAppointmentDTO)
        {
            int userId = userProviderService.GetUserId();
            try
            {
                int registrantId = userService.GetRegistrantIdByUserId(userId);
                appointmentService.AddAppointment(newAppointmentDTO, registrantId);
                return Ok("Wizyta zostala dodana pomyslnie");
            }
            catch (MedicalSystemServerException exeption)
            {
                return BadRequest(exeption.Message);
            }
        }

        [Authorize]
        [HttpPatch("doctor/appointment/{appointmentId}")]
        public IActionResult MakeAppointment([FromBody] AppointmentDTO newAppointmentDTO, int appointmentId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            {
                int doctorId = userService.GetDoctorIdByUserId(userId);
                appointmentService.MakeAppointment(newAppointmentDTO, appointmentId, doctorId);
                return Ok("Przeprowadzono wizyte pomyslnie");

            }
            else
            {
                return BadRequest("Brak uprawnien doktora");
            }
        }

        [Authorize]
        [HttpPatch("registrant/doctor/appointment/{appointmentId}")]
        public IActionResult CancelAppointment(int appointmentId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsRegistrant(userId) || userService.IsDoctor(userId))
            {
                appointmentService.CancelAppointment(appointmentId);
                return Ok("Anulowano wizyte pomyslnie");
            }
            else
            {
                return BadRequest("Brak uprawnien rejestratora");
            }
        }

        [Authorize]
        [HttpGet("doctor/appointment/{appointmentId}")]
        public IActionResult GetAppointment(int appointmentId)
        {
            int userId = userProviderService.GetUserId();
            if (userService.IsDoctor(userId))
            {
                return Ok(appointmentService.GetAppointment(appointmentId));

            }
            else
            {
                return BadRequest("Brak uprawnien lekarza");
            }
        }
    }
}
