using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.Services;
using MedicalSystemBackend.ServicesCore;
using MedicalSystemBackend.Settings;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace MedicalSystemBackend
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();

            DevelopmentSettings devSettings = new DevelopmentSettings();
            Configuration.GetSection("DevelopmentSettings").Bind(devSettings);
            if (devSettings.IsDevelopment)
            {
                services.AddMvc(option =>
                {
                    option.EnableEndpointRouting = false;
                    option.Filters.Add(new AllowAnonymousFilter());
                });
            }
            else
            {
                services.AddMvc(option =>
                {
                    option.EnableEndpointRouting = false;
                });
            }

            services.AddCors(options =>
            {
                CorsSettings corsSettings = new CorsSettings();
                Configuration.GetSection("CorsSettings").Bind(corsSettings);

                options.AddPolicy(
                    "default",
                    builder =>
                    {
                        builder
                        .WithOrigins(corsSettings.Origins)
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                    });
            });

            TokenSettings tokenSettings = new TokenSettings();
            Configuration.GetSection("TokenSettings").Bind(tokenSettings);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            }).AddJwtBearer("JwtBearer", jwtOptions =>
            {
                jwtOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSettings.SecretKey)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromHours(5),
                };
            });
            // services
            services.AddTransient<IUserProviderService, UserProviderService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITokenGeneratorService, TokenGeneratorService>();
            services.AddTransient<IUserRoleService, UserRoleService>();
            services.AddTransient<IAppointmentService, AppointmentService>();
            services.AddTransient<IAppointmentStatusService, AppointmentStatusService>();
            services.AddTransient<IPatientService, PatientService>();
            services.AddTransient<IDoctorService, DoctorService>();
            services.AddTransient<ILaboratoryTestService, LaboratoryTestService>();
            services.AddTransient<ILaboratoryTestStatusService, LaboratoryTestStatusService>();
            services.AddTransient<IPhysicalExaminationService, PhysicalExaminationService>();
            services.AddTransient<IExaminationService, ExaminationService>();

            services.AddHttpContextAccessor();
            services.AddSingleton<ITokenSettings>(tokenSettings);
            services.AddSingleton<IDevelopmentSettings>(devSettings);
            // database
            services.AddDbContextPool<MedicalSystemDbContext>(options => options.UseSqlServer(this.Configuration.GetConnectionString("MedicalSystemDBConnection")));
            // repositories
            services.AddScoped<IRepository<AppointmentDAO, int>, Repository<AppointmentDAO, int>>();
            services.AddScoped<IRepository<AppointmentStatusDAO, string>, Repository<AppointmentStatusDAO, string>>();
            services.AddScoped<IRepository<DoctorDAO, int>, Repository<DoctorDAO, int>>();
            services.AddScoped<IRepository<ExaminationDAO, string>, Repository<ExaminationDAO, string>>();
            services.AddScoped<IRepository<ExaminationTypeDAO, string>, Repository<ExaminationTypeDAO, string>>();
            services.AddScoped<IRepository<LaboratoryAssistantDAO, int>, Repository<LaboratoryAssistantDAO, int>>();
            services.AddScoped<IRepository<LaboratorySupervisorDAO, int>, Repository<LaboratorySupervisorDAO, int>>();
            services.AddScoped<IRepository<LaboratoryTestDAO, int>, Repository<LaboratoryTestDAO, int>>();
            services.AddScoped<IRepository<LaboratoryTestStatusDAO, string>, Repository<LaboratoryTestStatusDAO, string>>();
            services.AddScoped<IRepository<PatientDAO, int>, Repository<PatientDAO, int>>();
            services.AddScoped<IRepository<PhysicalExaminationDAO, int>, Repository<PhysicalExaminationDAO, int>>();
            services.AddScoped<IRepository<RegistrantDAO, int>, Repository<RegistrantDAO, int>>();
            services.AddScoped<IRepository<AdminDAO, int>, Repository<AdminDAO, int>>();
            services.AddScoped<IRepository<UserDAO, int>, Repository<UserDAO, int>>();
            services.AddScoped<IRepository<UserRoleDAO, string>, Repository<UserRoleDAO, string>>();
        }

        public void Configure(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<MedicalSystemDbContext>();
                context.Database.Migrate();
            }
            app.UseAuthentication();
            app.UseCors("default");
            app.UseRouting();
            app.UseMvc();
        }
    }
}
