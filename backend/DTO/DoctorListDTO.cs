﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class DoctorListDTO
    {
        public List<DoctorDTO> Doctors { get; set; }
        public DoctorListDTO()
        {
            Doctors = new List<DoctorDTO>();
        }
    }
}
