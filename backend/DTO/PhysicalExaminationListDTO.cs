﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class PhysicalExaminationListDTO
    {
        public List<PhysicalExaminationDTO> physicalExaminations { get; set; }
        public PhysicalExaminationListDTO()
        {
            physicalExaminations = new List<PhysicalExaminationDTO>();
        }
    }
}
