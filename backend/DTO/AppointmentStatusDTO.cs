﻿namespace MedicalSystemBackend.DTO
{
    public class AppointmentStatusDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
