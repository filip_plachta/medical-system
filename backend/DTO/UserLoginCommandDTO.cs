﻿namespace MedicalSystemBackend.DTO
{
    public class UserLoginCommandDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
