﻿namespace MedicalSystemBackend.DTO
{
    public class AppointmentDTO
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Diagnose { get; set; }
        public string Status { get; set; }
        public string RegistrationDate { get; set; }
        public string FinishedCancelledDate { get; set; }
        public int DoctorId { get; set; }
        public string DoctorName { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int RegistrantId { get; set; }
        public string RegistrantName { get; set; }
    }
}
