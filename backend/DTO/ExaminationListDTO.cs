﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class ExaminationListDTO
    {
        public List<ExaminationDTO> Examinations { get; set; }
        public ExaminationListDTO()
        {
            Examinations = new List<ExaminationDTO>();
        }
    }
}
