﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class AppointmentStatusListDTO
    {
        public List<AppointmentStatusDTO> AppointmentStatuses { get; set; }
        public AppointmentStatusListDTO()
        {
            AppointmentStatuses = new List<AppointmentStatusDTO>();
        }
    }
}
