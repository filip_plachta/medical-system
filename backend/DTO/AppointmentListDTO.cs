﻿using System.Collections.Generic;

namespace MedicalSystemBackend.DTO
{
    public class AppointmentListDTO
    {
        public List<AppointmentDTO> Appointments { get; set; }
        public AppointmentListDTO()
        {
            Appointments = new List<AppointmentDTO>();
        }
    }
}
