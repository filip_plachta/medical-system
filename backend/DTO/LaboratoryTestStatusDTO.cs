﻿namespace MedicalSystemBackend.DTO
{
    public class LaboratoryTestStatusDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
