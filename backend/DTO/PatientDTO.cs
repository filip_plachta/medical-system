﻿namespace MedicalSystemBackend.DTO
{
    public class PatientDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }

        public string PersonalIdentityNumber { get; set; }
    }
}
