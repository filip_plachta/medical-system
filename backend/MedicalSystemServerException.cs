﻿using System;

namespace MedicalSystemBackend
{
    public class MedicalSystemServerException : Exception
    {
        public MedicalSystemServerException(string statement) : base(statement)
        {

        }
    }
}
