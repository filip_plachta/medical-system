﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalSystemBackend.Model
{
    public class PatientDAO
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string FirstName { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Surname { get; set; }
        [Required]
        [Column(TypeName = "varchar(11)")]
        public string PersonalIdentityNumber { get; set; }

        public List<AppointmentDAO> Appointments { get; set; }
    }
}
