﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalSystemBackend.Model
{
    public class UserDAO
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string FirstName { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Surname { get; set; }
        [Required]
        [Column(TypeName = "varchar(25)")]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public string RoleCode { get; set; }
        public UserRoleDAO Role { get; set; }
    }
}
