﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalSystemBackend.Model
{
    public class DoctorDAO
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string LicenceNumber { get; set; }

        public int UserId { get; set; }
        public UserDAO User { get; set; }

        public List<AppointmentDAO> Appointments { get; set; }
    }
}
