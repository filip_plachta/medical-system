﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalSystemBackend.Model
{
    public class LaboratoryTestDAO
    {
        public int Id { get; set; }
        public string DoctorNote { get; set; }
        [Required]
        [Column(TypeName = "Date")]
        public DateTime OrderedDate { get; set; }
        public string Result { get; set; }
        [Column(TypeName = "Date")]
        public DateTime? ExecutionCancelledDate { get; set; }
        public string SupervisorNote { get; set; }
        [Column(TypeName = "Date")]
        public DateTime? AcceptionCancelledDate { get; set; }

        [Required]
        public string StatusCode { get; set; }
        public LaboratoryTestStatusDAO Status { get; set; }

        public int AppointmentId { get; set; }
        public AppointmentDAO Appointment { get; set; }

        [Required]
        public string ExaminationCode { get; set; }
        public ExaminationDAO Examination { get; set; }

        public int? LaboratoryAssistantId { get; set; }
        public LaboratoryAssistantDAO LaboratoryAssistant { get; set; }

        public int? LaboratorySupervisorId { get; set; }
        public LaboratorySupervisorDAO LaboratorySupervisor { get; set; }
    }
}
