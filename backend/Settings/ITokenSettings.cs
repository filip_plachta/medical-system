﻿namespace MedicalSystemBackend.Settings
{
    public interface ITokenSettings
    {
        string SecretKey { get; set; }
    }
}