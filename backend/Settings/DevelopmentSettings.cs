﻿namespace MedicalSystemBackend.Settings
{
    public class DevelopmentSettings : IDevelopmentSettings
    {
        public bool IsDevelopment { get; set; }
    }
}
