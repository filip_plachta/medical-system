﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface ILaboratoryTestStatusService
    {
        LaboratoryTestStatusListDTO GetLaboratoryTestStatuses();
    }
}
