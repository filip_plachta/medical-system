﻿using MedicalSystemBackend.DTO;
using System;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IAppointmentService
    {
        AppointmentListDTO GetAppointmentsByPatientId(int patientId);
        AppointmentListDTO GetAppointmentsByDate(DateTime date);
        AppointmentListDTO GetDoctorsAppointmentsByDate(DateTime date, int doctorId);
        AppointmentListDTO GetDoctorsAppointmentsByDateAndStatusCode(DateTime date, int doctorId, string statusCode);
        void AddAppointment(AppointmentDTO newAppointmentDTO, int registrantId);
        void MakeAppointment(AppointmentDTO appointmentDTO, int appointmentId, int doctorId);
        AppointmentDTO GetAppointment(int appointmentId);
        void CancelAppointment(int appointmentId);
    }
}
