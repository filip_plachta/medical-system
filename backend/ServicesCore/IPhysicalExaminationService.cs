﻿using MedicalSystemBackend.DTO;

namespace MedicalSystemBackend.ServicesCore
{
    public interface IPhysicalExaminationService
    {
        void AddPhysicalExamination(PhysicalExaminationDTO newPhysicalExaminationDTO);
        PhysicalExaminationListDTO GetPhysicalExaminationsByPatientId(int patientId);
    }
}
