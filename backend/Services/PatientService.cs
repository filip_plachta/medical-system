﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{
    public class PatientService : IPatientService
    {
        private readonly IRepository<PatientDAO, int> patientsRepository;

        public PatientService(IRepository<PatientDAO, int> patientsRepository)
        {
            this.patientsRepository = patientsRepository;
        }

        public PatientListDTO GetPatients()
        {
            IEnumerable<PatientDAO> patients = patientsRepository.GetAll();
            PatientListDTO patientslistDTO = new PatientListDTO();
            foreach (PatientDAO patient in patients)
            {
                patientslistDTO.Patients.Add(new PatientDTO
                {
                    Id = patient.Id,
                    FirstName = patient.FirstName,
                    Surname = patient.Surname,
                    PersonalIdentityNumber = patient.PersonalIdentityNumber
                });
            }
            return patientslistDTO;
        }

        public void AddPatient(PatientDTO newPatientDTO)
        {
            if (CheckIfPersonalIdentityNumberIsUnique(newPatientDTO.PersonalIdentityNumber))
            {
                PatientDAO patientDAO = new PatientDAO
                {
                    FirstName = newPatientDTO.FirstName,
                    Surname = newPatientDTO.Surname,
                    PersonalIdentityNumber = newPatientDTO.PersonalIdentityNumber
                };
                patientsRepository.Add(patientDAO);
            }
            else
            {
                throw new MedicalSystemServerException("Pacjent o takim numerze PESEL juz istnieje lub jego format jest niepoprawny");
            }
        }

        public bool CheckIfPersonalIdentityNumberIsUnique(string personalIdentityNumber)
        {
            if (personalIdentityNumber.Length != 11 && !int.TryParse(personalIdentityNumber, out _))
            {
                return false;
            }
            IEnumerable<PatientDAO> patients = patientsRepository.GetAll();
            foreach (PatientDAO patient in patients)
            {
                if (patient.PersonalIdentityNumber.Equals(personalIdentityNumber))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
