﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{
    public class LaboratoryTestStatusService : ILaboratoryTestStatusService
    {
        private readonly IRepository<LaboratoryTestStatusDAO, string> statusesRepository;

        public LaboratoryTestStatusService(IRepository<LaboratoryTestStatusDAO, string> statusesRepository)
        {
            this.statusesRepository = statusesRepository;
        }

        public LaboratoryTestStatusListDTO GetLaboratoryTestStatuses()
        {
            LaboratoryTestStatusListDTO laboratoryTestStatusListDTO = new LaboratoryTestStatusListDTO();
            IEnumerable<LaboratoryTestStatusDAO> statuses = statusesRepository.GetAll();
            foreach (LaboratoryTestStatusDAO status in statuses)
            {
                laboratoryTestStatusListDTO.Statuses.Add(new LaboratoryTestStatusDTO
                {
                    Code = status.Code,
                    Name = status.Name
                });
            }
            return laboratoryTestStatusListDTO;
        }
    }
}
