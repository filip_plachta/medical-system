﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System.Collections.Generic;

namespace MedicalSystemBackend.Services
{

    public class ExaminationService : IExaminationService
    {
        private readonly IRepository<ExaminationDAO, string> examinationRepository;

        public ExaminationService(IRepository<ExaminationDAO, string> examinationRepository)
        {
            this.examinationRepository = examinationRepository;
        }

        public ExaminationListDTO getExaminationsByType(string type)
        {
            IEnumerable<ExaminationDAO> examinations = examinationRepository.GetAll();
            ExaminationListDTO selectedExaminations = new ExaminationListDTO();
            foreach (ExaminationDAO examination in examinations)
            {
                if (examination.TypeCode == type)
                {
                    selectedExaminations.Examinations.Add(new ExaminationDTO
                    {
                        Code = examination.Code,
                        TypeCode = examination.TypeCode,
                        Name = examination.Name
                    });
                }
            }
            return selectedExaminations;
        }
    }
}
