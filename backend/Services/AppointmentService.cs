﻿using MedicalSystemBackend.DTO;
using MedicalSystemBackend.Model;
using MedicalSystemBackend.Repositories;
using MedicalSystemBackend.ServicesCore;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace MedicalSystemBackend.Services
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IRepository<AppointmentDAO, int> appointmentsRepository;
        private readonly IRepository<UserDAO, int> usersRepository;
        private readonly IRepository<UserRoleDAO, string> userRolesRepository;
        private readonly IRepository<DoctorDAO, int> doctorsRepository;
        private readonly IRepository<RegistrantDAO, int> registrantsRepository;
        private readonly IRepository<PatientDAO, int> patientsRepository;
        private readonly IRepository<AppointmentStatusDAO, string> appoinmentStatusRepository;

        public AppointmentService(
          IRepository<AppointmentDAO, int> appointmentsRepository,
          IRepository<UserDAO, int> usersRepository,
          IRepository<DoctorDAO, int> doctorsRepository,
          IRepository<RegistrantDAO, int> registrantsRepository,
          IRepository<PatientDAO, int> patientsRepository,
          IRepository<AppointmentStatusDAO, string> appoinmentStatusRepository)
        {
            this.appointmentsRepository = appointmentsRepository;
            this.usersRepository = usersRepository;
            this.doctorsRepository = doctorsRepository;
            this.registrantsRepository = registrantsRepository;
            this.patientsRepository = patientsRepository;
            this.appoinmentStatusRepository = appoinmentStatusRepository;
        }

        public AppointmentListDTO GetAppointmentsByPatientId(int patientId)
        {
            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            AppointmentListDTO appointmentsDTO = new AppointmentListDTO();
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.PatientId == patientId)
                {
                    UserDAO doctor = usersRepository.Get(doctorsRepository.Get(appointment.DoctorId).UserId);
                    UserDAO registrant = usersRepository.Get(registrantsRepository.Get(appointment.RegistrantId).UserId);
                    PatientDAO patient = patientsRepository.Get(appointment.PatientId);
                    appointmentsDTO.Appointments.Add(new AppointmentDTO
                    {
                        Id = appointment.Id,
                        Description = appointment.Description,
                        Diagnose = appointment.Diagnose,
                        Status = appointment.StatusCode,
                        RegistrationDate = appointment.RegistrationDate.ToString(),
                        FinishedCancelledDate = appointment.FinishedCancelledDate.ToString(),
                        DoctorId = doctor.Id,
                        DoctorName = doctor.FirstName + " " + doctor.Surname,
                        PatientId = patient.Id,
                        PatientName = patient.FirstName + " " + patient.Surname,
                        RegistrantId = registrant.Id,
                        RegistrantName = registrant.FirstName + " " + registrant.Surname

                    });
                }
            }
            return appointmentsDTO;
        }

        public AppointmentListDTO GetAppointmentsByDate(DateTime date)
        {
            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            AppointmentListDTO appointmentlistDTO = new AppointmentListDTO();
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.RegistrationDate.Date == date.Date)
                {
                    UserDAO doctor = usersRepository.Get(doctorsRepository.Get(appointment.DoctorId).UserId);
                    UserDAO registrant = usersRepository.Get(registrantsRepository.Get(appointment.RegistrantId).UserId);
                    PatientDAO patient = patientsRepository.Get(appointment.PatientId);
                    appointmentlistDTO.Appointments.Add(new AppointmentDTO
                    {
                        Id = appointment.Id,
                        Description = appointment.Description,
                        Diagnose = appointment.Diagnose,
                        Status = appointment.StatusCode,
                        RegistrationDate = appointment.RegistrationDate.ToString(),
                        FinishedCancelledDate = appointment.FinishedCancelledDate == null ? null : ((DateTime)appointment.FinishedCancelledDate).ToString("yyyy-MM-dd"),
                        DoctorId = doctor.Id,
                        DoctorName = doctor.FirstName + " " + doctor.Surname,
                        PatientId = patient.Id,
                        PatientName = patient.FirstName + " " + patient.Surname,
                        RegistrantId = registrant.Id,
                        RegistrantName = registrant.FirstName + " " + registrant.Surname
                    });
                }
            }
            return appointmentlistDTO;
        }
        public AppointmentListDTO GetDoctorsAppointmentsByDate(DateTime date, int doctorId)
        {
            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            AppointmentListDTO appointmentlistDTO = new AppointmentListDTO();
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.RegistrationDate.Date == date.Date && appointment.DoctorId == doctorId)
                {
                    UserDAO registrant = usersRepository.Get(registrantsRepository.Get(appointment.RegistrantId).UserId);
                    PatientDAO patient = patientsRepository.Get(appointment.PatientId);
                    appointmentlistDTO.Appointments.Add(new AppointmentDTO
                    {
                        Id = appointment.Id,
                        Description = appointment.Description,
                        Diagnose = appointment.Diagnose,
                        Status = appointment.StatusCode,
                        RegistrationDate = appointment.RegistrationDate.ToString(),
                        FinishedCancelledDate = appointment.FinishedCancelledDate == null ? null : ((DateTime)appointment.FinishedCancelledDate).ToString("yyyy-MM-dd"),
                        PatientId = patient.Id,
                        PatientName = patient.FirstName + " " + patient.Surname,
                        RegistrantId = registrant.Id,
                        RegistrantName = registrant.FirstName + " " + registrant.Surname
                    });
                }
            }
            return appointmentlistDTO;
        }

        public AppointmentListDTO GetDoctorsAppointmentsByDateAndStatusCode(DateTime date, int doctorId, string statusCode)
        {
            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            AppointmentListDTO appointmentlistDTO = new AppointmentListDTO();
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.RegistrationDate.Date == date.Date && appointment.DoctorId == doctorId && appointment.StatusCode == statusCode)
                {
                    UserDAO registrant = usersRepository.Get(registrantsRepository.Get(appointment.RegistrantId).UserId);
                    PatientDAO patient = patientsRepository.Get(appointment.PatientId);
                    appointmentlistDTO.Appointments.Add(new AppointmentDTO
                    {
                        Id = appointment.Id,
                        Description = appointment.Description,
                        Diagnose = appointment.Diagnose,
                        Status = appointment.StatusCode,
                        RegistrationDate = appointment.RegistrationDate.ToString(),
                        FinishedCancelledDate = appointment.FinishedCancelledDate == null ? null : ((DateTime)appointment.FinishedCancelledDate).ToString("yyyy-MM-dd"),
                        PatientId = patient.Id,
                        PatientName = patient.FirstName + " " + patient.Surname,
                        RegistrantId = registrant.Id,
                        RegistrantName = registrant.FirstName + " " + registrant.Surname
                    });
                }
            }
            return appointmentlistDTO;
        }
        public void AddAppointment(AppointmentDTO newAppointmentDTO, int registrantId)
        {
            DateTime registrationDate = DateTime.ParseExact(newAppointmentDTO.RegistrationDate, "yyyy/MM/dd HH:mm", CultureInfo.InvariantCulture);
            if (CheckIfNewAppointmentIsPossible(registrationDate, newAppointmentDTO.DoctorId))
            {
                AppointmentDAO appointmentDAO = new AppointmentDAO
                {
                    Description = newAppointmentDTO.Description,
                    RegistrationDate = registrationDate,
                    StatusCode = "REJ",
                    PatientId = newAppointmentDTO.PatientId,
                    DoctorId = newAppointmentDTO.DoctorId,
                    RegistrantId = registrantId,
                };
                appointmentsRepository.Add(appointmentDAO);
            }
            else
            {
                throw new MedicalSystemServerException("Termin wizyty jest juz zajety");
            }
        }

        public bool CheckIfNewAppointmentIsPossible(DateTime newAppointmentDate, int doctorId)
        {
            IEnumerable<AppointmentDAO> appointments = appointmentsRepository.GetAll();
            DateTime visitTime = new DateTime(1, 1, 1, 0, 30, 0); // 30 minut
            foreach (AppointmentDAO appointment in appointments)
            {
                if (appointment.DoctorId == doctorId &&
                    appointment.RegistrationDate.Date == newAppointmentDate.Date &&
                    (newAppointmentDate.TimeOfDay - appointment.RegistrationDate.TimeOfDay) < visitTime.TimeOfDay)
                {
                    return false;
                }
            }
            return true;
        }

        public void MakeAppointment(AppointmentDTO appointmentDTO, int appointmentId, int doctorId)
        {
            AppointmentDAO currentAppointment = appointmentsRepository.Get(appointmentId);
            appointmentsRepository.Update(new AppointmentDAO
            {
                Id = currentAppointment.Id,
                Description = currentAppointment.Description,
                Diagnose = appointmentDTO.Diagnose,
                RegistrationDate = currentAppointment.RegistrationDate,
                FinishedCancelledDate = DateTime.ParseExact(appointmentDTO.FinishedCancelledDate, "yyyy-mm-dd", CultureInfo.InvariantCulture),
                PatientId = currentAppointment.PatientId,
                RegistrantId = currentAppointment.RegistrantId,
                DoctorId = currentAppointment.DoctorId,
                StatusCode = "ZAK"
            });
        }

        public void CancelAppointment(int appointmentId)
        {
            AppointmentDAO currentAppointment = appointmentsRepository.Get(appointmentId);

            appointmentsRepository.Update(new AppointmentDAO
            {
                Id = currentAppointment.Id,
                Description = currentAppointment.Description,
                RegistrationDate = currentAppointment.RegistrationDate,
                PatientId = currentAppointment.PatientId,
                RegistrantId = currentAppointment.RegistrantId,
                DoctorId = currentAppointment.DoctorId,
                StatusCode = "ANU",
                FinishedCancelledDate = DateTime.Now
            });
        }

        public AppointmentDTO GetAppointment(int appointmentId)
        {
            AppointmentDAO appointmentDAO = appointmentsRepository.Get(appointmentId);
            PatientDAO patient = patientsRepository.Get(appointmentDAO.PatientId);
            AppointmentDTO appointmentDTO = new AppointmentDTO();
            appointmentDTO.Id = appointmentDAO.Id;
            appointmentDTO.PatientName = patient.FirstName + " " + patient.Surname;
            appointmentDTO.PatientId = patient.Id;
            appointmentDTO.Status = appointmentDAO.StatusCode;
            return appointmentDTO;
        }
    }
}
