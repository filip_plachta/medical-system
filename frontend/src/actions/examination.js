import axiosDefault from "./axiosConfiguration";

function getExaminations(type) {
  return axiosDefault({
    method: "GET",
    url: "/api/examination/doctor",
    params: {
      type,
    },
  });
}

export { getExaminations };
