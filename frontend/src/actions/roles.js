import axiosDefault from "./axiosConfiguration";

function getRoles() {
  return axiosDefault({
    method: "GET",
    url: '/api/userrole/admin',
  });
}

export { getRoles };