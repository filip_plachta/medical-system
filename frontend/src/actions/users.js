import axiosDefault from "./axiosConfiguration";

function getUsers() {
  return axiosDefault({
    method: "GET",
    url: "/api/user/admin",
  });
}

function getLoggedUserData() {
  return axiosDefault({
    method: "GET",
    url: "/api/user/myProfile"
  });
}

function addUser(data) {
  return axiosDefault({
    method: "POST",
    url: "/api/user/admin",
    data,
  });
}

function deactivateUser(id) {
  return axiosDefault({
    method: "DELETE",
    url: `/api/user/admin/deactivateuser/${id}`,
  });
}

function getUser(id) {
  return axiosDefault({
    method: "GET",
    url: `/api/user/admin/user/${id}`,
  });
}

function editUser(id, data) {
  return axiosDefault({
    method: "PATCH",
    url: `/api/user/admin/updateUser/${id}`,
    data,
  });
}

export { getUsers, getLoggedUserData, addUser, deactivateUser, getUser, editUser }