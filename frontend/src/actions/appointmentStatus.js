import axiosDefault from "./axiosConfiguration";

function getAppointmentStatuses() {
  return axiosDefault({
    method: "GET",
    url: "/api/appointmentStatus",
  });
}

export { getAppointmentStatuses };
