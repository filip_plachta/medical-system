import axiosDefault from "./axiosConfiguration";

function getAppointment(id) {
  return axiosDefault({
    method: "GET",
    url: `/api/appointment/doctor/appointment/${id}`,
  });
}

function makeAppointment(data, id) {
  return axiosDefault({
    method: "PATCH",
    url: `/api/appointment/doctor/appointment/${id}`,
    data,
  });
}

function getPatientAppointments(id) {
  return axiosDefault({
    method: "GET",
    url: `/api/appointment/doctor/patient/${id}`,
  });
}

function cancelAppointment(appointmentId){
  return axiosDefault({
    method: "PATCH",
    url: `api/appointment/registrant/doctor/appointment/${appointmentId}`,
  })
}

export { getAppointment, makeAppointment, getPatientAppointments, cancelAppointment };
